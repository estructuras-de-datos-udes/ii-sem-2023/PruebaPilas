/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Lab05pc05
 */
public class TestPila {
    
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        Stack<String> nombres = new Stack();
        String dato;
        
        do{
            System.out.println("Digite un nombre, si quiere salir digite .");
            dato = teclado.nextLine();
            
            if (dato.equals(".") != true) {
                nombres.push(dato);
            }
            
        }while(dato.equals(".") != true);
        
        imprimirPila(nombres);
//        while(nombres.empty() != true){
//            dato = nombres.pop();
//            System.out.println(dato);
//        }
    }
    
        private static <T> void imprimirPila(Stack<T> pila){
         while(pila.empty() != true){
            T dato = pila.pop();
            System.out.println(dato);
        }
    }
}
