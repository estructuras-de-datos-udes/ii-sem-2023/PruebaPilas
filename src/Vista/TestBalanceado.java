/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Lab05pc05
 */
public class TestBalanceado {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        Stack<Character> expresion = new Stack();
        Stack<Character> contenido = new Stack();
        
        System.out.println("Digite una expresión con parentesis balanceados");
        String dato = teclado.nextLine();
        
        expresion = getPila(dato);
        contenido = getPila(dato);
        
        if (isBalanceado(expresion)) {
            System.out.println("Tiene parentesis balanceados");
        }else{
            System.out.println("No tiene parentesis balanceados");
        }
        
        imprimirPila(contenido);
        
    }
    
    private static Stack<Character> getPila(String cadena){
        char vector[] = cadena.toCharArray();
        Stack<Character> pila = new Stack();
        
        for (int i = 0; i < vector.length; i++) {
            pila.push(vector[i]);
        }
        return pila;
    }

    private static <T> void imprimirPila(Stack<T> pila){
         while(pila.empty() != true){
            T dato = pila.pop();
            System.out.println(dato);
        }
    }
    
    private static boolean isBalanceado(Stack<Character> pila){
        Stack<Character> contenedor = new Stack();
        
        while(!pila.empty()){
            if (pila.peek()== ')') {
                pila.pop();
                contenedor.push(')');
            }
            else if (pila.peek() == '('){
                if (!contenedor.empty()) {
                    pila.pop();
                    contenedor.pop();
                }else{
                    return false;
                }
            }
            else{
                pila.pop();
            }
        }
        
        if (contenedor.empty()) {
            return true;
        }
        return false;
    }
}
